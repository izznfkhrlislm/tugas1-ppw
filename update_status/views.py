from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import UpdateStatus_Form
from .models import Status
from halaman_profile.models import AccountProfile

# Create your views here.
response = {'author': "Kelompok 4 PPW-F"}

def index(request):
	html = 'update_status.html'
	response['name'] = AccountProfile.objects.get(id=1)
	status = Status.objects.all()
	response['status'] = status
	response['updatestatus_form'] = UpdateStatus_Form
	return render(request, html, response)

def status_post(request):
	form = UpdateStatus_Form(request.POST)
	if request.method == 'POST' and form.is_valid():
		response['status'] = request.POST['status'] #if request.POST['name'] != ""
		status = Status(status=response['status'])
		status.save()
		html = 'update_status.html'
		render(request, html, response)
		return HttpResponseRedirect('/update-status/')
	else:
		return HttpResponseRedirect('/update-status/')