from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Status
from .forms import UpdateStatus_Form
from halaman_profile.models import AccountProfile

# Create your tests here.
class UpdateStatusUnitTest(TestCase):

	def test_update_status_url_is_exist(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code, 200)

	def test_update_status_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	def test_update_status_using_update_status_template(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/update-status/')
		self.assertTemplateUsed(response, 'update_status.html')

	def test_model_can_create_new_status(self):
		#Create a new status
		new_status = Status.objects.create(status='Sedang sibuk mengerjakan tugas 1 PPW')

		#Retrieving all available activity
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)

	def test_update_status_post_success_and_render_the_result(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response_post = Client().post('/update-status/add_status', {'status':'lagi nyoba update status'})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertIn('lagi nyoba update status', html_response)

	def test_form_validation_for_blank_items(self):
		form = UpdateStatus_Form(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'], ["This field is required."])

	def test_post_error_and_render_the_result(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		test = 'Anonymous'
		response_post = Client().post('/update-status/add_status', {'status': ''})
		self.assertEqual(response_post.status_code, 302)
		
		response= Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)