from django.shortcuts import render, redirect, get_object_or_404
from .models import NewFriend
from .forms import Friend_Form
from datetime import datetime
from django.http import HttpResponseRedirect
from halaman_profile.models import AccountProfile

# Create your views here.
response = {'author': "Kelompok 4 PPW-F"}

def index(request):
	html = 'add_friend.html'
	response['name'] = AccountProfile.objects.get()
	friends = NewFriend.objects.all()
	response['friends'] = friends
	response['friend_form'] = Friend_Form
	return render(request, html, response)
	
def add_friend(request):
	form = Friend_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		new_friend = NewFriend(name=response['name'],url=response['url'])
		new_friend.save()
		return HttpResponseRedirect('/add-friend/')
	else:
		return HttpResponseRedirect('/add-friend/')

def delete_friend(request, id=None):
	obj = get_object_or_404(NewFriend, pk=id)
	obj.delete()
	return HttpResponseRedirect('/add-friend/')
