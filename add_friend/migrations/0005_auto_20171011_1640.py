# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 09:40
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('add_friend', '0004_auto_20171011_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newfriend',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 11, 16, 40, 3, 880242, tzinfo=utc)),
        ),
    ]
