# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 16:45
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('add_friend', '0009_merge_20171011_2345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newfriend',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 11, 23, 45, 11, 701477, tzinfo=utc)),
        ),
    ]
