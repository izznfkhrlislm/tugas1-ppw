from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    name_attrs = { 
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Abraham Lincoln'
    }
    url_attrs = {
        'type': 'text',
        'cols': 50, 
        'rows': 4,
        'class': 'form-control',
        'placeholder':'thisisabraham.herokuapp.com'
    }

    name = forms.CharField(label='Friend Name:', required=False, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    url = forms.URLField(label ='Friend URL:',required=True ,widget=forms.TextInput(attrs=url_attrs))
