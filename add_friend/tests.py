from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_friend, delete_friend
from .models import NewFriend
from .forms import Friend_Form
from halaman_profile.models import AccountProfile

# Create your tests here.
class AddFriendUnitTest(TestCase):

	def test_add_friend_url_is_exist(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)
	
	def test_add_friend_using_add_friend_template(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/add-friend/')
		self.assertTemplateUsed(response, 'add_friend.html')

	def test_model_can_create_new_add_friend(self):
		#Creating a new activity
		new_add_friend = NewFriend.objects.create(name = "haii",url='www.facebook.com')

		#Retrieving all available activity
		counting_all_available_friend = NewFriend.objects.all().count()
		self.assertEqual(counting_all_available_friend,1) 

	def test_add_friend_post_success_and_render_the_result(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		test = "Michael ABCD"
		test_url = "thisisabc.herokuapp.com"
		response_post = Client().post('/add-friend/add_friend', {'name': test, 'url': test_url})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
		self.assertIn(test_url, html_response)

	def test_add_friend_post_error_and_render_the_result(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		test = 'Anonymous'
		test_url = "haii.com"
		response_post = Client().post('/add-friend/add_friend',{'name': '', 'url': ''})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/add-friend/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)
		self.assertNotIn(test_url, html_response)

	def test_form_validation_for_blank_items(self):
		form = Friend_Form(data={'name': '', 'url': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['url'],["This field is required."])

	def test_for_unfriend(self):
		unfriend = NewFriend.objects.create(name = 'remove name', url="remove.com")
		all_friend = NewFriend.objects.all().count()
		delete_friend(self, unfriend.id)
		check_all_available_unfriend = NewFriend.objects.all().count()
		self.assertEqual(check_all_available_unfriend, all_friend-1)