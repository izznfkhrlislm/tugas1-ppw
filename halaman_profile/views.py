from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import AccountProfile

# Create your views here.
# response = {'author': 'Kelompok 4 PPW-F'}
#expertise = [
#    'Marketing',
#    'Collector',
#    'Public Speaking',
#]
#name = 'Hepzibah Smith'
#birth = '01 Jan'
#gender = 'Female'
#description = 'Antique expertise'
#email = 'hello@smith.com'

response = {'author': 'Kelompok 4 PPW-F'}
def index(request):
	try:
		default = AccountProfile.objects.get(id=1)
	except:
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expert", email="hello@smith.com", id=1)
		default.save()
	finally:
		default = AccountProfile.objects.get(id=1)
		splitexp = default.expertise.split(',')
		# response['name'] = AccountProfile.objects.get()
		response['default'] = default
		response['expertise'] = splitexp
		return render(request, 'halaman_profile.html', response)

#Hanya untuk menampilkan page edit profile
def edit_profile(request):
	response['name'] = AccountProfile.objects.get(id=1)
	accountprofile = AccountProfile.objects.all()
	response['accountprofile'] = accountprofile
	return render(request, 'edit_profile.html', response)

def post_custom_profile(request):
	if request.method == 'POST' and form.is_valid():
		response['name'] = request.POST['name'] #if request.POST['name'] != ""
		response['birthday'] = request.POST['birthday']
		response['gender'] = request.POST['gender']
		response['expertise'] = request.POST['expertise']
		response['description'] = request.POST['description']
		response['email'] = request.POST['email']
		newProfile = AccountProfile(name=response['name'],birthday=response['birthday'],gender=response['gender'],expertise=response['expertise'],description=response['description'],email=response['email'])
		newProfile.save()
		html = 'edit_profile.html'
		render(request, html, response)
		return HttpResponseRedirect('/halaman-profile/')
	else:
		return HttpResponseRedirect('/edit-profile/')

#jika ingin submit profile setelah di edit
def submit(request):
	getnew = AccountProfile.objects.get(id=1)
	getnew.name = request.POST['name']
	getnew.birthday = request.POST['birthday']
	getnew.gender = request.POST['gender']
	getnew.expertise = request.POST['expertise']
	getnew.description = request.POST['description']
	getnew.email = request.POST['email']
	getnew.save()
	# render(request, 'halaman-profile.html', response)
	return HttpResponseRedirect('/halaman-profile/')