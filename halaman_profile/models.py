from django.db import models
from django.shortcuts import get_object_or_404

# Create your models here.
class AccountProfile(models.Model):
    name = models.CharField(max_length=30)
    birthday = models.CharField(max_length=30)
    gender = models.CharField(max_length=30)
    expertise = models.CharField(max_length=140)
    description = models.CharField(max_length=140)
    email = models.EmailField()
    # picturl = models.URLField(max_length=200)

    def __str__(self):
    	return self.name

    def getBirthday(self):
        return self.birthday

    def getGender(self):
        return self.gender

    def getExpertise(self):
        return self.expertise

    def getDescription(self):
        return self.description

    def getEmail(self):
        return self.email                   