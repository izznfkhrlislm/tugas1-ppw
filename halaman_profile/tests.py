from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import AccountProfile

# Create your tests here.
class HalamanProfileUnitTest(TestCase):

	#Ini masih error. TODO: Bikin file halaman_profile.html di halaman_profile/templates
	def test_halaman_profile_url_is_exist(self):
		response = Client().get('/halaman-profile/')
		self.assertEqual(response.status_code, 200)

	def test_halaman_profile_using_index_func(self):
		found = resolve('/halaman-profile/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_acc(self):
        # Creating a new account
		new_account = AccountProfile.objects.create(name='Hepzibah Smith', birthday='01 Jan', 
		gender='Female', expertise='Marketing, Collector, Public Speaking', description='Antique expert', 
		email='hello@smith.com')

		# Retrieving all available account
		count_all_available_account = AccountProfile.objects.all().count()
		self.assertEqual(count_all_available_account, 1)
		self.assertEqual('Hepzibah Smith', str(new_account))
		self.assertEqual('01 Jan', new_account.getBirthday())
		self.assertEqual('Female', new_account.getGender())
		self.assertEqual('Marketing, Collector, Public Speaking', new_account.getExpertise())
		self.assertEqual('Antique expert', new_account.getDescription())
		self.assertEqual('hello@smith.com', new_account.getEmail())

	def test_model_can_store_data(self):
		# Creating a new account
		new_account = AccountProfile.objects.create(name='Hepzibah Smith', birthday='01 Jan', 
		gender='Female', expertise='Marketing, Collector, Public Speaking', description='Antique expert', 
		email='hello@smith.com')
		account_name = new_account.name
		self.assertEqual(account_name, 'Hepzibah Smith')

	# def test_profile_user_expertise_in_profile_page(self):
	# 	exp = AccountProfile(expertise='ngoding')
	# 	exp.save()
	# 	found = resolve('/%s/' % self.account_name)
	# 	self.assertEqual(found.func, index)
	# 	response = Client().get('/%s/' % self.account_name)
	# 	html_resp = response.content.decode('utf-8')
	# 	self.assertIn(exp.expertise, html_resp)

	def test_edit_profile_using_edit_profile_template(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/edit_profile/')
		self.assertTemplateUsed(response, 'edit_profile.html')

	def test_edit_profile_using_edit_profile_template(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/halaman-profile/edit_profile')
		html_response = response.content.decode('utf8')
		self.assertIn('Edit Profile', html_response)

	def test_update_status_post_success_and_render_the_result(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response_post = Client().post('/halaman-profile/submit', {"name":"Hepzibah Smith", "birthday":"01 Jan", "gender":"Famale", "expertise":"Marketing, Collector, Public Speaking", "description":"Antique expertise", "email":"hello@smith.com"})
		self.assertEqual(response_post.status_code, 302)