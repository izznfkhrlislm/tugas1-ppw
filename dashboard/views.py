from django.shortcuts import render
from halaman_profile.models import AccountProfile
from update_status.models import Status
from add_friend.models import NewFriend

# Create your views here.
response = {'author': "Kelompok 4 PPW-F"}

def index(request):
	html = 'dashboard.html'
	response['name'] = AccountProfile.objects.get(id=1)
	response['statusCount'] = Status.objects.all().count()
	response['friendsCount'] = NewFriend.objects.all().count()
	response['latestPost'] = Status.objects.last()
	return render(request, html, response)