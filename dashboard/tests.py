from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from halaman_profile.models import AccountProfile
from .views import index

# Create your tests here.
class DashboardUnitTest(TestCase):

	def test_dashboard_url_is_exist(self):
		default = AccountProfile.objects.create(name="Hepzibah Smith", birthday="01 Jan", gender="Famale", expertise="Marketing, Collector, Public Speaking", description="Antique expertise", email="hello@smith.com", id=1)
		response = Client().get('/dashboard/')
		self.assertEqual(response.status_code, 200)

	def test_dashboard_using_index_func(self):
		found = resolve('/dashboard/')
		self.assertEqual(found.func, index)