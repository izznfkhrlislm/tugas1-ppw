[![pipeline status](https://gitlab.com/izznfkhrlislm/tugas1-ppw/badges/master/pipeline.svg)](https://gitlab.com/izznfkhrlislm/tugas1-ppw/commits/master)
[![coverage report](https://gitlab.com/izznfkhrlislm/tugas1-ppw/badges/master/coverage.svg)](https://gitlab.com/izznfkhrlislm/tugas1-ppw/commits/master)

Web Design & Programming Project 1
====
## Group 4 (Class F) - 2017/2018 Term 3
Project Members:
1. Izzan Fakhril Islam (1606875806) [@izznfkhrlislm](https://gitlab.com/izznfkhrlislm) - Statistics, Dashboard, Navigation Bar, Footer
2. Raihan Mahendra Sutanto (1606917992) [@raihanmhndr](https://gitlab.com/raihanmhndr) - Update Status
3. Irfani Ramadianti (1606918295) [@irfanidianti](https://gitlab.com/irfanidianti) - Profile
4. Ihsan Putrananda (1606918276) [@ihsanputra66](https://gitlab.com/ihsanputra66) - Add Friend

**Heroku App Link: ppw-tugas1-4f.herokuapp.com**

